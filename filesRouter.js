const express = require('express');
const fs = require('fs');
const path = require('path');
const dirExists = require('./middleware/dirExists');
const validateParams = require('./middleware/validateParams');

const filesRouter = express.Router();
const DIR = './createdFiles';

filesRouter.use(dirExists(DIR));

filesRouter.get('/', (_, res) => {
  const files = fs.readdirSync(DIR);
  res.json({
    message: 'Success',
    files,
  });
});

filesRouter.get('/:filename', (req, res) => {
  const { filename } = req.params;
  const files = fs.readdirSync(DIR);
  if (!files.includes(filename)) {
    res.status(400).json({
      message: `No file with '${filename}' name found`,
    });
    return;
  }

  const filePath = `${DIR}/${filename}`;

  fs.readFile(filePath, 'utf-8', (err, data) => {
    if (err) {
      res.status(500).json({ message: 'server error' });
      return;
    }

    const stats = fs.statSync(filePath);
    const extension = path.extname(filename).replace('.', '');

    res.status(200).json({
      message: 'Success',
      filename,
      content: data,
      extension,
      uploadedDate: stats.mtime,
    });
  });
});

// will rewrite in case file already exists
filesRouter.post('/', validateParams, (req, res) => {
  const { filename, content } = req.body;

  fs.writeFile(`${DIR}/${filename}`, content, (err) => {
    if (err) {
      res.status(500).json({ message: 'server error' });
      return;
    }

    res.status(200).json({
      message: 'File created successfully',
    });
  });
});

// will append content to file
filesRouter.put('/:filename', (req, res) => {
  const { filename } = req.params;
  const { content } = req.body;

  if (!content) {
    return res.status(400).json({ message: 'Please specify \'content\' parameter' });
  }

  fs.appendFile(`${DIR}/${filename}`, content, (err) => {
    if (err) {
      return res.status(500).json({ message: 'server error' });
    }

    res.json({ message: `File with ${filename} updated` });
  });
});

filesRouter.delete('/:filename', (req, res) => {
  const { filename } = req.params;

  fs.unlink(`${DIR}/${filename}`, (err) => {
    if (err) {
      if (err.code === 'ENOENT') {
        return res.status(400).json({ message: `File with '${filename}' name was not found` });
      }

      return res.status(500).json({ message: 'server error' });
    }

    res.status(200).json({ message: 'File was deleted' });
  });
});

module.exports = { filesRouter };
