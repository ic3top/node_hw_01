const express = require('express');
const morgan = require('morgan');
const { filesRouter } = require('./filesRouter');

const PORT = process.env.PORT || 8080;
const app = express();

app.use(morgan('tiny'));
app.use(express.json());

app.use('/api/files', filesRouter);

app.listen(PORT);
