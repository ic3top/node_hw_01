const fs = require('fs');

module.exports = function dirExists(dir) {
  return (req, res, next) => {
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    next();
  };
};
