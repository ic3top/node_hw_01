module.exports = (req, res, next) => {
  const { filename, content } = req.body;
    
  if (!filename) {
    return res.status(400).json({ message: 'Please specify \'filename\' parameter' });
  }

  if (!content) {
    return res.status(400).json({ message: 'Please specify \'content\' parameter' });
  }

  next();
};
